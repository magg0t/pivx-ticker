# PIVX - Ticker
## Created By: magg0t

## Details
------
This script will update your website with the latest market data for the PIVX crypto currency.The API updates every 5 minutes and so the ticker follows suit and also updates automaticallyevery 5 minutes.

## Installation
------
To install simply include the `ticker.js` and `style.css` in your web directory and properly link to them in your html.

## Use
------
To use the PIVX Ticker all you need to do is create a `<div>` with an id of `pivx-ticker` like so `<div id="pivx-ticker"></div>`. The script will seek out this div and populate it with results from the API.

## Credits
------
Special thanks to the PIVX community on Slack and @ https://pivx.org and to the CoinMarketCap JSON API @ http://coinmarketcap-nexuist.rhcloud.com/

Tips: [DK96maaWEGfwfw8EdzmHJJKyxEZttg211z](pivx:DK96maaWEGfwfw8EdzmHJJKyxEZttg211z)
